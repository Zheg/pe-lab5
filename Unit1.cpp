//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FindBClick(TObject *Sender)
{
	if (db.count(SurnameFind->Text)) {
		FindRes->Caption = IntToStr(db.at(SurnameFind->Text));
	} else {
		FindRes->Caption = "There is not person with this surname";
    }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::AddBClick(TObject *Sender)
{
	db[SurnameAdd->Text] = StrToInt(FlatAdd->Text);
	SurnameAdd->Text = "ADDED";
	FlatAdd->Text = "ADDED";
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ChangeBClick(TObject *Sender)
{
	if (db.count(SurnameChange->Text)) {
		db[SurnameChange->Text] = StrToInt(FlatChange->Text);
		SurnameChange->Text = "CHANGED";
		FlatChange->Text = "CHANGED";
	} else {
		SurnameChange->Text = "There is not person with this surname";
	}

}
//---------------------------------------------------------------------------
void __fastcall TForm1::SurnameAddClick(TObject *Sender)
{
	SurnameAdd->Text = "";
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FlatAddClick(TObject *Sender)
{
	FlatAdd->Text = "";
}
//---------------------------------------------------------------------------
void __fastcall TForm1::SurnameChangeClick(TObject *Sender)
{
	SurnameChange->Text = "";
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FlatChangeClick(TObject *Sender)
{
	FlatChange->Text = "";
}
//---------------------------------------------------------------------------

void __fastcall TForm1::RemoveBClick(TObject *Sender)
{
	if (db.count(SurnameRemove->Text)) {
		db.erase(SurnameRemove->Text);
		SurnameRemove->Text = "REMOVED";
	} else {
		SurnameRemove->Text = "There is not person with this surname";
    }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::SurnameRemoveClick(TObject *Sender)
{
	SurnameRemove->Text = "";
}
//---------------------------------------------------------------------------

