object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 721
  ClientWidth = 534
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 24
    Width = 25
    Height = 17
    Caption = 'Find:'
  end
  object Label2: TLabel
    Left = 24
    Top = 184
    Width = 23
    Height = 13
    Caption = 'Add:'
  end
  object Label3: TLabel
    Left = 24
    Top = 344
    Width = 41
    Height = 13
    Caption = 'Change:'
  end
  object Label4: TLabel
    Left = 48
    Top = 56
    Width = 46
    Height = 13
    Caption = 'Surname:'
  end
  object Label5: TLabel
    Left = 72
    Top = 128
    Width = 22
    Height = 13
    Caption = 'Flat:'
  end
  object FindRes: TLabel
    Left = 112
    Top = 128
    Width = 30
    Height = 13
    Caption = 'Result'
  end
  object Label6: TLabel
    Left = 48
    Top = 216
    Width = 46
    Height = 13
    Caption = 'Surname:'
  end
  object Label7: TLabel
    Left = 72
    Top = 243
    Width = 22
    Height = 13
    Caption = 'Flat:'
  end
  object Label8: TLabel
    Left = 48
    Top = 376
    Width = 46
    Height = 13
    Caption = 'Surname:'
  end
  object Label9: TLabel
    Left = 72
    Top = 403
    Width = 22
    Height = 13
    Caption = 'Flat:'
  end
  object Label10: TLabel
    Left = 24
    Top = 496
    Width = 39
    Height = 13
    Caption = 'Remove'
  end
  object Label11: TLabel
    Left = 48
    Top = 528
    Width = 46
    Height = 13
    Caption = 'Surname:'
  end
  object SurnameFind: TEdit
    Left = 112
    Top = 53
    Width = 97
    Height = 21
    TabOrder = 0
  end
  object FindB: TButton
    Left = 72
    Top = 88
    Width = 75
    Height = 25
    Caption = 'Find'
    TabOrder = 1
    OnClick = FindBClick
  end
  object SurnameAdd: TEdit
    Left = 112
    Top = 213
    Width = 97
    Height = 21
    TabOrder = 2
    OnClick = SurnameAddClick
  end
  object FlatAdd: TEdit
    Left = 112
    Top = 240
    Width = 97
    Height = 21
    TabOrder = 3
    OnClick = FlatAddClick
  end
  object AddB: TButton
    Left = 72
    Top = 280
    Width = 75
    Height = 25
    Caption = 'Add'
    TabOrder = 4
    OnClick = AddBClick
  end
  object SurnameChange: TEdit
    Left = 112
    Top = 373
    Width = 97
    Height = 21
    TabOrder = 5
    OnClick = SurnameChangeClick
  end
  object FlatChange: TEdit
    Left = 112
    Top = 400
    Width = 97
    Height = 21
    TabOrder = 6
    OnClick = FlatChangeClick
  end
  object ChangeB: TButton
    Left = 72
    Top = 440
    Width = 75
    Height = 25
    Caption = 'Change'
    TabOrder = 7
    OnClick = ChangeBClick
  end
  object SurnameRemove: TEdit
    Left = 112
    Top = 525
    Width = 97
    Height = 21
    TabOrder = 8
    OnClick = SurnameRemoveClick
  end
  object RemoveB: TButton
    Left = 72
    Top = 568
    Width = 75
    Height = 25
    Caption = 'Remove'
    TabOrder = 9
    OnClick = RemoveBClick
  end
end
