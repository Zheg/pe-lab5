//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>

#include <map>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TLabel *Label1;
	TLabel *Label2;
	TLabel *Label3;
	TEdit *SurnameFind;
	TLabel *Label4;
	TLabel *Label5;
	TLabel *FindRes;
	TButton *FindB;
	TEdit *SurnameAdd;
	TLabel *Label6;
	TEdit *FlatAdd;
	TLabel *Label7;
	TButton *AddB;
	TEdit *SurnameChange;
	TLabel *Label8;
	TEdit *FlatChange;
	TLabel *Label9;
	TButton *ChangeB;
	TLabel *Label10;
	TEdit *SurnameRemove;
	TLabel *Label11;
	TButton *RemoveB;
	void __fastcall FindBClick(TObject *Sender);
	void __fastcall AddBClick(TObject *Sender);
	void __fastcall ChangeBClick(TObject *Sender);
	void __fastcall SurnameAddClick(TObject *Sender);
	void __fastcall FlatAddClick(TObject *Sender);
	void __fastcall SurnameChangeClick(TObject *Sender);
	void __fastcall FlatChangeClick(TObject *Sender);
	void __fastcall RemoveBClick(TObject *Sender);
	void __fastcall SurnameRemoveClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;

std::map <UnicodeString, int> db;
//---------------------------------------------------------------------------
#endif
